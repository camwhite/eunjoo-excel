﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ClosedXML.Excel;
using System.Diagnostics;
using System.IO;
using System.Xml.Linq;

namespace eunjoo_excel
{
    class Program
    {
        public const string dir = @"..\..\..\input\";
        public string csvInputFileName = "munged_CCA_CCS_FPA_FPS_high-combined-FPKM.csv";
        public string xmlInputFileName = "munged_CCA_CCS_FPA_FPS_high-combined-FPKM.xlsx";
        public string inputPath, convertedPath;

        public Program ()
        {
            inputPath = dir + csvInputFileName;
            convertedPath = dir + xmlInputFileName;
        }

        static void Main(string[] args)
        {
            var p = new Program();

            var paths = Directory.GetFiles(dir);
            List<Dictionary<string, List<double>>> mapList = new List<Dictionary<string, List<double>>>();
            string geneName = string.Empty;
            var workBook = new XLWorkbook();
            foreach (var csvFile in paths)
            {
                // Header row
                IXLRow headerRow;
                var dict = p.GenerateHashMap(csvFile, out headerRow);
                mapList.Add(dict);

                // Write out to XLSX file
                var index = mapList.IndexOf(dict);

                
                
                
                // Stopped here to implement the selection dialog
                // Select the gene
                geneName = mapList[index].First().Key;




                var worksheet = workBook.Worksheets.Add(geneName);


                worksheet.Cell(1, 1).Value = Path.GetFileName(csvFile);
                worksheet.Cell(1, 2).Value = headerRow; // Set the header
                worksheet.Cell(2, 2).Value = geneName;
                for (int i = 1; i < worksheet.ColumnsUsed().Count() - 1; i++)
                {
                    worksheet.Cell(2, i + 2).Value = mapList[0][geneName][i - 1];
                }
                
            }

            workBook.SaveAs(Program.dir + geneName + ".xlsx");

            /*
            * Here we have the hash table and the headerRow to work with
            * The following will create the output file from the conglomeration of each of the CSV files
            */


            // Select the gene



        }

        private Dictionary<string, List<double>> GenerateHashMap(string inputCSVFile, out IXLRow headerRow)
        {
            var xlsxFileName = string.Empty;
            xlsxFileName = Path.ChangeExtension(inputCSVFile, ".xlsx");
            var csvLines = ReadCsv(inputCSVFile, ',');
            
            var workbook = this.CovertToWorkbook(xlsxFileName, "1", csvLines);

            // Generate list from 1st column
            var workSheet = workbook.Worksheet(1);
            var columnOne = workSheet.Column(1);    // Select the 1st column, from the 1st worksheet in the workbook
            Dictionary<string, List<double>> hashTable = new Dictionary<string, List<double>>();
            List<string> geneNames = new List<string>();
            var colOneCells = columnOne.Cells(2, columnOne.Cells().Count());
            foreach (var name in colOneCells) // Loop through the 2nd to the last row in the 1st column
            {
                geneNames.Add(name.RichText.Text);
                hashTable.Add(name.RichText.Text, new List<double>());
            }

            // Get the header row
            headerRow = workSheet.Row(1);
            // Get the row for each gene
            foreach (var row in workSheet.Rows(2, workSheet.Rows().Count()))
            {

                // Find the current gene
                var geneName = row.Cell(1).RichText.Text;
                List<double> list = new List<double>();
                // Pick out the set of colums consisting of the 2nd column to the end column
                foreach (var cell in row.Cells(2, row.Cells().Count()))    // Get all of it's columns
                {
                    double cellValue = double.NaN;

                    if (double.TryParse(cell.RichText.Text, out cellValue))
                    {
                        list.Add(cellValue);
                    }

                } // End of loop which traverses each row's cells

                hashTable[geneName] = list;
                // Write out the row to a new workbook's worksheet
            } // End of loop which traverses each workbooks rows

            return hashTable;
        }

        /// <summary>
        /// TODO: Make generic so that it can covert all files from a given directory
        /// </summary>
        /// <param name="excelFileName"></param>
        /// <param name="worksheetName"></param>
        /// <param name="csvLines"></param>
        /// <returns></returns>
        private XLWorkbook CovertToWorkbook(string excelFileName, string worksheetName, IEnumerable<string[]> csvLines)
        {
            if (csvLines == null || csvLines.Count() == 0)
            {
                return null;
            }

            int rowCount = 0;
            int colCount = 0;

            using (var workbook = new XLWorkbook())
            {
                using (var worksheet = workbook.Worksheets.Add(worksheetName))
                {
                    rowCount = 1;
                    foreach (var line in csvLines)
                    {
                        colCount = 1;
                        foreach (var col in line)
                        {
                            worksheet.Cell(rowCount, colCount).Value = TypeConverter.TryConvert(col);
                            colCount++;
                        }
                        rowCount++;
                    }

                }
                workbook.SaveAs(excelFileName);
                return workbook;
            }
        }

        private static IEnumerable<string[]> ReadCsv(string fileName, char delimiter = ';')
        {
            var lines = System.IO.File.ReadAllLines(fileName, Encoding.UTF8).Select(a => a.Split(delimiter));
            return (lines);
        }
    }
}
