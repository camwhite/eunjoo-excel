﻿using ClosedXML.Excel;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;
using System.Collections;
using System.Reflection;
using System.Threading;

namespace GUI
{
    public partial class Form1 : Form
    {
        RowEntry r = null;
        Program p = null;
        /// <summary>
        /// Duplicate genes entry rows from the excel files
        /// </summary>
        List<Program.DuplicateEventArgs> m_currentDuplicates = new List<Program.DuplicateEventArgs>();
        // Create an instance of a ListView column sorter and assign it 
        // to the ListView control.
        ListViewColumnSorter lvwColumnSorter = new ListViewColumnSorter();

        /// <summary>
        /// Used by Program.GenerateHashmap to change the input file type
        /// </summary>
        public bool CSV { get { return tsmiCSV.Checked; } }
        /// <summary>
        /// Used by Program.GenerateHashmap to change the input file type
        /// </summary>
        public bool XLSX { get { return tsmiXLSX.Checked; } }

        public Form1()
        {  
            InitializeComponent();
            AttachClickEventListeners();
            
        }

        /// <summary>
        /// AttachClickEventListenersToSubMenuItems
        /// </summary>
        private void AttachClickEventListeners()
        {
            // Go through the horizontal menu items
            foreach (var tsmi in mnuMain.Items.Cast<ToolStripMenuItem>())
            {
                // In each, go through their dropdown menu items
                foreach (var dropDownItem in tsmi.DropDownItems.Cast<ToolStripMenuItem>())
                {
                    // Add click event handlers
                    dropDownItem.Click += ToggleMenuItems;
                }
            }

            tsmiCSV.CheckedChanged += tsmi_CheckedChanged;
            tsmiXLSX.CheckedChanged += tsmi_CheckedChanged;
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            p = new Program();
            p.GUI = this;
            p.DuplicateAdded += OnDuplicateEntryAdded;
            r = new RowEntry();
            m_lvDuplicates.ListViewItemSorter = this.lvwColumnSorter;
        }

        private void OnDuplicateEntryAdded(object sender, Program.DuplicateEventArgs e)
        {
            m_tsslError.Image = GUI.Properties.Resources.error;
            m_currentDuplicates.Add(e);
            //currentDuplicates = e.duplicate;
            m_tsslError.Text = "A duplicate gene exists! Click to toggle details";
            this.Invalidate();
        }

        private void GetInputFiles ()
        {
            var paths = new List<string>();
            foreach (var filePath in Directory.GetFiles(Program.inputDir))
            {                            // Get the paths of the files from the input directory
                var fileName = Path.GetFileName(filePath);                                              // Get the file name from the path
                if (fileName.StartsWith("~"))                                                           // If the file is a temporary Excel file
                    continue;                                                                           // Do not pass temporary Excel files GenerateHashmap
                paths.Add(filePath);                                                                    // Otherwise remember this file as legit
            }
            r.inputFilePaths = paths.ToArray();                                                         // Cast the file list to an array
        }
        /// <summary>
        /// Generates a hashmap for each file in the input directory and populates the dropdown with that file's contents
        /// </summary>
        private async void PopulateDropDown()
        {      
            GetInputFiles();
            foreach (var inputFile in r.inputFilePaths) {                                               // Once per file
                var fileExtension = Path.GetExtension(inputFile);                                       // Get the file's extension type
                if (fileExtension == ".xlsx" && this.CSV ||                                             // If there's a mismatch in the option selected and the files present
                     fileExtension == ".csv" && this.XLSX)  
                {
                    MessageBox.Show("The file type in the input directory does not match the selected InputFileType." +
                        "Please either put the correct file type in the directory, or select the correct file type from the dropdown.",
                        "Filetype mismatch",
                        MessageBoxButtons.OK,
                        MessageBoxIcon.Information);
                    m_cmbBox.Items.Clear();                                                             // Clear the combo box as we have no data to work with
                    return;                                                                             // Exit gracefully without crashing
                }
                m_tsslError.Text = "Processing File: " + inputFile;
                this.Invalidate();
                //this.Width = TextRenderer.MeasureText(m_tsslStatus.Text + m_tsslError.Text, m_tsslError.Font).Width;
                ToggleGUIState(false);
                try {
                    var hashResult = await Task.Run( () => 
                        p.GenerateHashMap(inputFile));               // Otherwise we have a legit file type selection and files present, generate the hashmap per file
                    r.headerRows.Add(hashResult.headerRow);                                                            // Track the header row of each file
                    r.mapList.Add(hashResult.Result);                                                                    // Keep a list of the hashmaps ordered by file order
                    ResetListedComboItems(0);                                                               // Recreate the dropdown list from this hashmap
                    RefreshDuplicatesListView();
                }
                catch (IOException e)
                {
                    MessageBox.Show(e.Message, "Please close all excel file during list generation", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    ToggleGUIState(true);
                    m_cmbBox.Items.Clear();
                    return;
                }
                catch (ArgumentException e)
                {
                    var response = MessageBox.Show(e.Message + "Would you like to skip this file?", 
                        "Please provide files with unique gene names before using this program", MessageBoxButtons.YesNo, MessageBoxIcon.Error);

                    if (response == DialogResult.No)
                    {
                        ToggleGUIState(true);
                        m_cmbBox.Items.Clear();
                        return;
                    }
                    else
                    {
                        continue;                                                                   // Skip this file and try the next
                    }
                }
                catch (Exception e)
                {
                    MessageBox.Show(e.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    ToggleGUIState(true);
                    m_cmbBox.Items.Clear();
                    return;
                }
            }
            m_tsslError.Text = "Done";
            ToggleGUIState(true);          
        }

        /// <summary>
        /// Enabled or diabled the GUI based on the input boolean
        /// </summary>
        /// <param name="enabled">Whether the GUI should be enabled</param>
        private void ToggleGUIState (bool enabled)
        {
            if (enabled)
            {
                this.Cursor = Cursors.Arrow;                                                             // Notify the user of operation success
                mnuMain.Enabled = m_btnGenerateGeneList.Enabled = m_cmbBox.Enabled = true;               // Allow file type changes again
            }

            else
            {
                this.Cursor = Cursors.WaitCursor;                                                        // Notify the user that this will take some time
                m_btnGenerateGeneList.Enabled = mnuMain.Enabled = m_cmbBox.Enabled = false;                 // Disable the button to prevent duplicate calls
            }
        }
        /// <summary>
        /// Re-creates the combo box list from the current hashmap
        /// </summary>
        /// <param name="index">The index into the list of hashmaps to use; Usually 0</param>
        private void ResetListedComboItems(int index)
        {
            m_cmbBox.Items.Clear();
            m_cmbBox.Items.AddRange(r.mapList[index].Keys.ToArray());
        }

        /// <summary>
        /// Object representing a row entry in the new Excel file; Used to pass data between the m_cmbBox_SelectedIndexChanged and PopulateDropDown
        /// </summary>
        public class RowEntry
        {
            /// <summary>
            /// A list of the header rows ordered by
            /// </summary>
            public List<IXLRow> headerRows = new List<IXLRow>();
            /// <summary>
            /// The gene name selected in the combo dropdown
            /// </summary>
            public string geneName = string.Empty;
            /// <summary>
            /// The file paths for files in the input directory (Program.inputDir)
            /// </summary>
            public string[] inputFilePaths = null;
            /// <summary>
            /// The list of hashmaps; One for each file
            /// </summary>
            public List<Dictionary<string, List<double>>> mapList = new List<Dictionary<string, List<double>>>();
        }

        private void m_cmbBox_TextChanged(object sender, EventArgs e)
        {
            // get the keyword to search
            string textToSearch = m_cmbBox.Text.ToLower();
            if (String.IsNullOrEmpty(textToSearch))
                ResetListedComboItems(0);
            //search
            string[] result = (from i in r.mapList[0].Keys
                                where i.ToLower().Contains(textToSearch)
                                select i).ToArray();
            if (result.Length == 0)
                return;

            m_cmbBox.Items.Clear();
            m_cmbBox.Items.AddRange(result);
            
            m_cmbBox.SelectionStart = m_cmbBox.Text.Length;
        }

        private void m_cmbBox_SelectionChangeCommitted(object sender, EventArgs e)
        {
            if (m_cmbBox.SelectedItem != null)                                                          // Make sure we have a selection
            {
                var workBook = new XLWorkbook();                                                        // Create the new XLSX file

                // Select the gene
                r.geneName = m_cmbBox.SelectedItem.ToString();                                          // Grab the gene name from the combo box

                var worksheet = workBook.Worksheets.Add(r.geneName);                                    // Create a worksheet with this gene name

                for (int i = 1; i < r.inputFilePaths.Length + 1; i++)                                   // Once for each file in Program.inputDir
                {
                                                                                                        // Add to the odd rows in this sheet; Can be though of as skipping one row
                    var inputFile = r.inputFilePaths[i - 1];                                            // Write out the file path
                    worksheet.Cell((i * 2) - 1, 1).Value = Path.GetFileName(inputFile);                 // Write out the file name 
                    worksheet.Cell((i * 2) - 1, 2).Value = r.headerRows[i - 1];                         // Write out the header row
                    worksheet.Cell(i * 2, 2).Value = r.geneName;                                        // Write out the gene name 

                    var valuesList = r.mapList[i - 1][r.geneName];                                      // Get the list of values for the gene represented by this file
                    for (int j = 1; j < valuesList.Count + 1; j++)                                      // Once for each value
                    {
                        worksheet.Cell(i * 2, j + 2).Value = valuesList[j - 1];                         // Write out the value on the even rows of this worksheet starting with the 3rd column

                                                                                                        // Set the number formatting                                                                     
                        if (j < 5)                                                                      // For the next 4 columns (C-F)
                            worksheet.Cell(i * 2, j + 2).Style.NumberFormat.Format = "0.00";            // Use 2 decimal points
                        // G-N (5 - 12) #8
                        else if (j < 13)                                                                // For the next 8 columns (G-N)
                            worksheet.Cell(i * 2, j + 2).Style.NumberFormat.Format = "0.0000";          // Use 4 decimal points
                        // O-T (13-18) #6
                        else if (j < 19)                                                                // For the next 6 columns (O-T) and any column after these
                            worksheet.Cell(i * 2, j + 2).Style.NumberFormat.Format = "0.000";           // Use 3 decimal points
                    }                                                                                   // End of looping over each gene's list of values
                }                                                                                       // End of looping over each file

                workBook.SaveAs(Program.outputDir + r.geneName + ".xlsx");                              // Save the workbook in Program.outputDir, using the gene name, and an '.xlsx' extension
                ResetListedComboItems(0);                                                               // Repopulate the dropdown, otherwise the list will only list the previously selected gene
            }
        }

        /// <summary>
        /// Allows only one menu item to be checked at once
        /// </summary>
        /// <param name="sender">The menu item that was clicked</param>
        /// <param name="e"></param>
        private void ToggleMenuItems(object sender, EventArgs e)
        {
            var itemClicked = sender.CastTo<ToolStripMenuItem>();                                       // We know the sender is a ToolStripMenuItem so cast it
            foreach (var tsmi in tsmiInputFileType.DropDownItems.Cast<ToolStripMenuItem>())             // Once for each TSMI in our InputFileType menu
            {
                if (tsmi == itemClicked)                                                                // If this was our sender (the item that was clicked)
                    tsmi.Checked = true;                                                                // Check the thing we clicked on
                else                                                                                    // For all other TSMIs in our menu
                    tsmi.Checked = false;                                                               // Uncheck all the others
            }
        }

        private void m_btnGenerateGeneList_Click(object sender, EventArgs e)
        {
            PopulateDropDown();                                                                         // Fill the dropdown by generating a hashmap for each file
        }

        private void tsmi_CheckedChanged(object sender, EventArgs e)
        {
            var tsmi = sender as ToolStripMenuItem;
            if (tsmi.Checked) {
                r.headerRows.Clear();                                                                   // Clear the memory from previous hashings
                r.mapList.Clear();                                                                      // "    "
                m_btnGenerateGeneList.Enabled = true;                                                   // Allow user to re create hashmap
            }
        }

        private void m_btnGenerateColumnList_Click(object sender, EventArgs e)
        {
            List<HashMapResult> list = new List<HashMapResult>();
            var newFile = new XLWorkbook();
            var newSheet = newFile.Worksheets.Add("1");
            GetInputFiles();
            // Create workbook from each input file
            foreach (var inputFile in r.inputFilePaths) {
                var workbook = CreateWorkbookAsync(inputFile).Result;
                var worksheet = workbook.Worksheet(1);
                // Extract the necessary columns (B, RPKM/FPKM) into a new workbook/worksheet
                var columnB = worksheet.Column("B").CellsUsed();
                var columnRPKM = worksheet.Column("Q").CellsUsed();
                newSheet.Column("A").Cell(1).Value = worksheet.Column("A");
                newSheet.Column("B").Cell(1).Value = worksheet.Column("B");
                newSheet.Column("C").Cell(1).Value = worksheet.Column("Q");

                // Generate the hashmap from the workbook
                var hashMapRes = p.GenerateHashMap(newFile);
                list.Add(hashMapRes);
            }


            newFile.SaveAs(Program.outputDir + "output.xlsx");
            // Populate the dropdown
        }

        private async Task<XLWorkbook> CreateWorkbookAsync (string inputFile)
        {
            XLWorkbook workbook = null;                                             // Initialize a new workbook
            if (this.CSV)                                                       // If we are given CSV input
                workbook = p.CovertToWorkbook(inputFile, "1",                         // Covert the CSV file by looking at it's 1st worksheet
                    Program.ReadCsv(inputFile, ','));                                       // Split the CSV file into a string array
            else if (this.XLSX)                                                 // If we're given an XLSX file
                workbook = await NewWorkbook(inputFile);                               // Straight up, create the workbook from that filename

            return workbook;
        }

        private async Task<XLWorkbook> NewWorkbook(string inputFile)
        {
            return new XLWorkbook(inputFile);
        }

        private void m_tsslError_Click(object sender, EventArgs e)
        {
            if (m_lvDuplicates.Visible || m_tsslError.Image == null)        // If you can see me, or if our state is not an error state
                m_lvDuplicates.Visible = false;                             // Toggle closed
            else
            {
                m_lvDuplicates.Visible = true;                              // Otherwise we're either invisible, 
                                                                            // or in an error state, in both cases show popup
                RefreshDuplicatesListView();
            }
        }

        private void RefreshDuplicatesListView()
        {
            m_lvDuplicates.Items.Clear();
            foreach (var row in m_currentDuplicates)
            {
                var lvi = new ListViewItem(row.file.Name);                  // file name
                lvi.SubItems.Add(new ListViewItem.
                    ListViewSubItem(lvi, row.row.RowNumber().ToString()));  // Row number
                lvi.SubItems.Add(new ListViewItem.
                    ListViewSubItem(lvi, row.geneName));                    // Gene name
                m_lvDuplicates.Items.Add(lvi);
            }
        }

        private void m_lvDuplicates_ColumnClick(object sender, ColumnClickEventArgs e)
        {
            // Determine if clicked column is already the column that is being sorted.
            if (e.Column == lvwColumnSorter.SortColumn)
            {
                // Reverse the current sort direction for this column.
                if (lvwColumnSorter.Order == SortOrder.Ascending)
                {
                    lvwColumnSorter.Order = SortOrder.Descending;
                }
                else
                {
                    lvwColumnSorter.Order = SortOrder.Ascending;
                }
            }
            else
            {
                // Set the column number that is to be sorted; default to ascending.
                lvwColumnSorter.SortColumn = e.Column;
                lvwColumnSorter.Order = SortOrder.Ascending;
            }

            // Perform the sort with these new sort options.
            this.m_lvDuplicates.Sort();
        }
    }
}
